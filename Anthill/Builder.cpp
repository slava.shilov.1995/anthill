#include "Builder.h"
#include "Ant.h"
#include "Material.h"

Object * GetNearestMaterial(Cell(&field)[Settings::N][Settings::N], Ant * me, int & foodX, int & foodY) {
	int range = 2;
	for (int y = me->y - range; y < me->y + range; ++y) {
		for (int x = me->x - range; x < me->x + range; ++x) {
			if (me->IsValidCoords(x, y)) {
				if (field[x][y].object != nullptr) {
					if (field[x][y].object->type == 1) {
						foodX = x;
						foodY = y;
						return field[x][y].object;
					}
				}
			}
		}
	}
	return nullptr;
}

int Builder::Work(Cell(&field)[Settings::N][Settings::N], Ant * me) {
	if (materialCount >= Settings::baggageForFood || me->age - 10 < me->DistanceTo(me->home->x, me->home->y)) {
		int tx, ty;
		if (me->FindPath(field, 0, INT_MAX, tx, ty)) {
			me->Move(field, tx, ty);
			if (me->DistanceTo(me->home->x, me->home->y) <= 2) {
				me->home->capacity += materialCount;
				materialCount = 0;
			}
		}
		return 0;
	}

	int tx, ty;
	if (me->FindPath(field, 2, Settings::findFoodDistance, tx, ty)) {
		me->Move(field, tx, ty);
		Object * target = GetNearestMaterial(field, me, tx, ty);
		if (target != nullptr) {
			materialCount += target->getMass();
			field[tx][ty].object->Destroy();
			field[tx][ty].object = nullptr;
		}
		return 0;
	}

	/*if (me->FindPath(field, 0, INT_MAX, tx, ty)) {
	me->Move(field, tx, ty);
	if (me->DistanceTo(me->home->x, me->home->y) <= 2) {
	me->home->food += foodCount;
	foodCount = 0;
	}
	}*/
	return 0;
}