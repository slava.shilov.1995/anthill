#include "Queen.h"
#include "Cell.h"
#include "Ant.h"

void RandomHomeCoords(int sx, int sy, int & ox, int & oy) {//startX : outX
	int range = 200;
	do {
		ox = sx - range + rand() % (range * 2);
		oy = sy - range + rand() % (range * 2);
	} while (!(ox >= 0 && oy >= 0 && ox < Settings::N && oy < Settings::N));
	
}

int Queen::Work(Cell(&field)[Settings::N][Settings::N], Ant * me) {
	if (home == nullptr) {
		if (me->x != homeX || me->y != homeY) {
			if (me->IsValidCoords(homeX, homeY)) {
				int tx = homeX;
				int ty = homeY;
				if (me->FindPath(field, 4, INT_MAX, tx, ty)) {
					me->Move(field, tx, ty);
					if (me->InDanger(field, homeX, homeY)) {
						RandomHomeCoords(me->x, me->y, homeX, homeY);
					}
				} else {
					RandomHomeCoords(me->x, me->y, homeX, homeY);
				}
			} else {
				RandomHomeCoords(me->x, me->y, homeX, homeY);
			}
		} else {
			if (field[me->x][me->y].object == nullptr) {
				home = new Anthill(me->x, me->y);
				me->home = home;
				field[me->x][me->y].object = home;
				home->color = me->color;
				me->isBrave = true;
				home->capacity = Settings::startAnthillCapacity;
				home->food = me->satiety - 1;
				me->satiety = 1;
				home->queen = me;
				home->inhabitants.push_back(me);
			} else {
				RandomHomeCoords(me->x, me->y, homeX, homeY);
			}
		}
	} else {//if we already in own anthill
		if (me->x != homeX || me->y != homeY) {
			int tx, ty;
			if (me->FindPath(field, 0, INT_MAX, tx, ty)) {
				me->Move(field, tx, ty);
			}
		} else {//everyday queen life 
			--pregnantTime;
			if (pregnantTime <= 0) {
				if (home->food >= Settings::antQueenPregnantTime) {
					home->lavraStock.Add();
					pregnantTime = Settings::antQueenPregnantTime;
					home->food -= Settings::antQueenPregnantTime;
				}
			}
		}
	}
	
	return 0;
}