#pragma once
#include "Anthill.h"
#include "Job.h"
#include "Homeless.h"
#include "Queen.h"
#include "Cell.h"

class Ant{
public:
	int x;
	int y;
	int age;
	int lifeTime;
	int health;
	int satiety;
	bool isBrave;
	Anthill * home;
	Job * job;

	bool lived;

	sf::Color color;

	Ant(int _x, int _y, int _satiety, Anthill * _home, Job * _job, int _lifetime) : x(_x), y(_y), satiety(_satiety), home(_home), job(_job) {
		health = 100;
		age = 0;
		lifeTime = _lifetime;
		isBrave = false;
		if (home != nullptr) {
			color = home->color;
			home->inhabitants.push_back(this);
			if (lifeTime == Settings::antQueenLife) {
				home->queen = this;
				isBrave = true;
			}
		} else {
			color = sf::Color(rand() % 255, rand() % 255, rand() % 255);
			job = new Queen(nullptr);
		}
		if (job == nullptr) {
			job = new Homeless();
		}
		

		lived = false;
	}
	~Ant() {
		job->Destroy();
		if (home != nullptr) {
			for (std::vector<Ant*>::iterator it = home->inhabitants.begin(); it < home->inhabitants.end(); ++it) {
				if ((*it) == this) {
					home->inhabitants.erase(it);
				}
			}
		}
	}

	bool InDanger(Cell(&field)[Settings::N][Settings::N], int & toX, int & toY);//check for danger ant return flag and relative coords to run away

	//mode - looking for: 0 - home, 1 - food, 2 - materials, 3 - other anthills, 4 - input coords
	bool FindPath(Cell (&field)[Settings::N][Settings::N], int mode, int maxDistance, int & toX, int & toY);
	void GoEat(Cell(&field)[Settings::N][Settings::N]);
	int DistanceTo(int x1, int y1);
	int Live(Cell (&field) [Settings::N][Settings::N]);
	void Move(Cell(&field)[Settings::N][Settings::N], int toX, int toY);
	bool IsValidCoords(int x, int y);


};