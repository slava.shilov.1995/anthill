#pragma once
#include "Job.h"
class Queen : public Job {
private:
	
public:
	Anthill * home;
	int homeX;
	int homeY;

	int pregnantTime;

	virtual int Work(Cell(&field)[Settings::N][Settings::N], Ant * me);
	

	Queen(Anthill * _home) {
		home = _home;
		pregnantTime = 0;
		if (home == nullptr) {
			homeX = -1;
			homeY = -1;
		} else {
			homeX = home->x;
			homeY = home->y;
		}
	}
	~Queen() {
		if (home != nullptr) {
			std::cout << "queen died ";
			home->queen = nullptr;
		}
	}
	virtual void Destroy() { delete this; }
	virtual int GetType() { return 0; }
};