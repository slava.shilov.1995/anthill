#include "Field.h"
#include "Food.h"
#include "Material.h"
#include "Ant.h"

#include <random>
#include <iostream>

Field::Field() {
	for (int y = 0; y < Settings::N; ++y) {
		for (int x = 0; x < Settings::N; ++x) {
			field[x][y].ant = nullptr;
			field[x][y].object = nullptr;
		}
	}

	for (int i = 0; i < Settings::startQueenCount; ++i) {
		int x, y;
		GetFreeCoords(0, x, y);
		field[x][y].ant = new Ant(x, y, Settings::startQueenFood, nullptr, nullptr, Settings::antQueenLife);
	}
}

void Field::GetFreeCoords(int matrixIndex, int & x, int & y) {
	int tx, ty;
	do {
		tx = rand() % Settings::N;
		ty = rand() % Settings::N;
	} while (matrixIndex ? (field[tx][ty].object != nullptr) : (field[tx][ty].ant != nullptr));
	x = tx;
	y = ty;
}

void Field::Step() {
	for (int y = 0; y < Settings::N; ++y) {
		for (int x = 0; x < Settings::N; ++x) {
			if (field[x][y].ant != nullptr) {
				field[x][y].ant->lived = false;
			}
		}
	}

	int foodCount = 0;
	int materialsCount = 0;
	for (int y = 0; y < Settings::N; ++y) {
		for (int x = 0; x < Settings::N; ++x) {
			if (field[x][y].ant != nullptr) {
				if (!field[x][y].ant->lived) {
					int liveResult = field[x][y].ant->Live(field);
					if (liveResult == 1) {
						delete field[x][y].ant;
						field[x][y].ant = nullptr;
					}
				}
			}
			if (field[x][y].object != nullptr) {
				switch (field[x][y].object->type) {
				case 0: ++foodCount; break;
				case 1: ++materialsCount; break;
				case 2: 
					field[x][y].object->Interact(field); 
					int result = field[x][y].object->getMass();
					
					if (result <= 0) {
						field[x][y].object->Destroy();
						field[x][y].object = nullptr;
					}
					break;
				}
			}
		}
	}
	if (foodCount < Settings::foodUnitsCount) {
		int x, y;
		GetFreeCoords(1, x, y);
		field[x][y].object = new Food(x, y);
	}
	if (materialsCount < Settings::materialUnitsCount) {
		int x, y;
		GetFreeCoords(1, x, y);
		field[x][y].object = new Material(x, y);
	}
}