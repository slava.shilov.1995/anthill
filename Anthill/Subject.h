#pragma once
#include <vector>
class Subject {
protected:
	int x;
	int y;
public:
	virtual int Live() = 0;
};