#pragma once
#include "Job.h"
class Getter : public Job {
private:
	int foodCount;


public:
	/*int Bring() {};
	int Get() {};
	int SearchForFood() {};*/
	Getter() {
		foodCount = 0;
	}

	virtual int Work(Cell(&field)[Settings::N][Settings::N], Ant * me);
	virtual void Destroy() { delete this; }
	virtual int GetType() { return 1; }
};