#include "Homeless.h"

#include "Ant.h"
#include "Cell.h"

int Homeless::Work(Cell(&field)[Settings::N][Settings::N], Ant * me) {
	int toX = me->x - 2 + rand() % 6;
	int toY = me->y - 2 + rand() % 6;
	if (me->FindPath(field, 4, 4, toX, toY)) {
		me->Move(field, toX, toY);
	}
	return 0;
}