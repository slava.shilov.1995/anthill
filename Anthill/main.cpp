#include "Field.h"
//#include "Object.h"
#include "Ant.h"

#include <SFML/Graphics.hpp>
#include <thread>
#include <mutex>
#include <random>
#include <time.h>
#include <iostream>

using namespace std;

bool shuttingDown = false;
mutex fieldMutex;

void Draw(Field * model) {
	sf::RenderWindow window(sf::VideoMode(Settings::windowSize, Settings::windowSize), "Okay, gents, ANTS!");
	float cellSize = (float)Settings::windowSize / (float)Settings::N;

	sf::RectangleShape background1(sf::Vector2f(cellSize, cellSize));
	background1.setFillColor(sf::Color(76, 204, 0));
	sf::RectangleShape background2(sf::Vector2f(cellSize, cellSize));
	background2.setFillColor(sf::Color(145, 218, 0));
	window.clear();
	for (int y = 0; y < Settings::N; ++y) {
		for (int x = 0; x < Settings::N; ++x) {
			if (((x + y) % 2) == 0) {
				background1.setPosition(cellSize * x, cellSize * y);
				window.draw(background1);
			} else {
				background2.setPosition(cellSize * x, cellSize * y);
				window.draw(background2);
			}
		}
	}
	sf::Texture texture;
	texture.create(Settings::windowSize, Settings::windowSize);
	texture.update(window);
	sf::Sprite background(texture);

	sf::CircleShape ant(cellSize / 2);
	ant.setOutlineColor(sf::Color(0, 0, 0));
	ant.setOutlineThickness(1);

	int objectSize = 3;//bigger int -> smaller object
	int anthillSize = 15;

	sf::CircleShape anthillShape(cellSize * 2);
	anthillShape.setOrigin(cellSize * 2, cellSize * 2);
	anthillShape.setFillColor(sf::Color(151, 95, 45, 150));
	//anthillShape.setOutlineThickness(1);

	sf::CircleShape foodShape(cellSize / objectSize);
	foodShape.setOrigin(cellSize / objectSize, cellSize / objectSize);
	foodShape.setFillColor(sf::Color(255, 0, 0));
	//foodShape.setOutlineColor(sf::Color(0, 0, 0));
	//foodShape.setOutlineThickness(0.5);


	sf::CircleShape materialShape(cellSize / objectSize);
	materialShape.setOrigin(cellSize / objectSize, cellSize / objectSize);
	materialShape.setFillColor(sf::Color(20, 20, 20));

	

	while (window.isOpen() && !shuttingDown) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				break;
			}
		}

		window.clear();
		window.draw(background);
		fieldMutex.lock();

		for (int y = 0; y < Settings::N; ++y) {
			for (int x = 0; x < Settings::N; ++x) {
				if (model->field[x][y].object != nullptr) {
					if (model->field[x][y].object->type == 2) {
						anthillShape.setRadius(cellSize / 2 + model->field[x][y].object->getMass() / anthillSize);
						anthillShape.setOrigin(anthillShape.getRadius(), anthillShape.getRadius());
						if (model->field[x][y].ant != nullptr) {
							anthillShape.setOutlineThickness(1);
							anthillShape.setOutlineColor(model->field[x][y].ant->color);
						} else {
							anthillShape.setOutlineThickness(0);
						}
						anthillShape.setPosition(cellSize * x + cellSize / 2, cellSize * y + cellSize / 2);
						window.draw(anthillShape);
					}
				}
			}
		}
		
		for (int y = 0; y < Settings::N; ++y) {
			for (int x = 0; x < Settings::N; ++x) {
				if (model->field[x][y].object != nullptr) {
					if (model->field[x][y].object->type == 1) {
						materialShape.setPosition(cellSize * x + cellSize / 2, cellSize * y + cellSize / 2);
						window.draw(materialShape);
					} else if (model->field[x][y].object->type == 0) {
						foodShape.setPosition(cellSize * x + cellSize / 2, cellSize * y + cellSize / 2);
						window.draw(foodShape);
					}
				}
				if (model->field[x][y].ant != nullptr) {
					ant.setPosition(cellSize * x, cellSize * y);
					ant.setFillColor(model->field[x][y].ant->color);
					window.draw(ant);
				}
			}
		}
		fieldMutex.unlock();
		window.display();
		//cout << "draw" << endl;
	}
	if (!shuttingDown) {
		//cout << "trying to exit but cin.get() is still waiting for you <3" << endl;
	}
	shuttingDown = true;
	cin.unget();
	
}

void LoopModel(Field * model) {
	int step = 0;
	while (!shuttingDown) {
		fieldMutex.lock();
		model->Step();
		fieldMutex.unlock();
		cout << "step " << (step++) << endl;
		this_thread::sleep_for(chrono::milliseconds(Settings::stepTime));
	}
}

int main() {
	srand((int)time(NULL));

	Field * model = new Field;

	cout << "Press ESC to shut this ants down." << endl;

	std::thread draw(Draw, model);
	std::thread life(LoopModel, model);
	
	//cin.get();
	//shuttingDown = true;
	life.join();
	draw.join();

	return 0;
}