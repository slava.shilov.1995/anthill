#pragma once

#include "Anthill.h"
class Cell;
class Ant;

class Job {
private:
	
public:
	virtual int Work(Cell (&field)[Settings::N][Settings::N], Ant * me) = 0;
	virtual void Destroy() = 0;
	virtual int GetType() = 0;
	
	Job() {};
	~Job() { };
};