#pragma once
namespace Settings {
	constexpr int N = 128;
	constexpr int windowSize = 896;// 640;
	constexpr int stepTime = 100;

	constexpr int startQueenCount = 5;
	constexpr int startQueenFood = 1200;
	constexpr int startAnthillCapacity = 500;

	constexpr int foodUnitsCount = 10;
	constexpr int foodMinQuantity = 1000;
	constexpr int foodRandomRange = 200;

	constexpr int materialUnitsCount = 10;
	constexpr int materialMinQuantity = 15;
	constexpr int materialRandomRange = 10;

	constexpr int anhillBadFoodLevel = 200;

	constexpr int antLife = 500;
	constexpr int antQueenLife = 600;
	constexpr int antQueenPregnantTime = 20;
	constexpr int lavraLiveTime = 30;

	constexpr int dangerViewRadius = 5;

	constexpr int antMaxSatiety = 150;

	constexpr int baggageForFood = 110; // capacity of food
	constexpr int findFoodDistance = 100;//distance to search for food

	constexpr int baggageForMaterials = 30; // capacity of materials

	constexpr int cover = 10; //additional defence for defender
	constexpr int at = 10; //additional defence for defender

}