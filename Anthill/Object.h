#pragma once
#include "Settings.h"

#include <iostream>
#include <random>

class Cell;

class Object {
public:
	int x;
	int y;
	int type;

	virtual void Interact(Cell(&field)[Settings::N][Settings::N]) = 0;
	virtual int getMass() = 0;

	Object() {};
	~Object() {};
	virtual void Destroy() = 0;
};