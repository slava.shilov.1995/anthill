#pragma once
#include "Object.h"

class Food : public Object {
public:
	int caloricity;

	Food(int _x, int _y) {
		x = _x;
		y = _y;
		type = 0;
		caloricity = Settings::foodMinQuantity + rand() % Settings::foodRandomRange;
	}
	Food(int _x, int _y, int _caloricity) {
		x = _x;
		y = _y;
		type = 0;
		caloricity = _caloricity;
	}
	~Food() {};

	void Interact(Cell(&field)[Settings::N][Settings::N]) {}
	int getMass() { return caloricity; }
	void Destroy() { delete this; }
};