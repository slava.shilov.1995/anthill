#include "Ant.h"
#include <queue>
#include <iostream>

bool Ant::IsValidCoords(int x, int y) {
	return (x >= 0 && y >= 0 && x < Settings::N && y < Settings::N);
}

int Ant::Live(Cell (&field) [Settings::N][Settings::N]) {
	lived = true;
	++age;
	--satiety;
	if (age > lifeTime || satiety < 0) {
		return 1;
	}
	if (home != nullptr) {
		if (DistanceTo(home->x, home->y) > satiety - 20) {
			GoEat(field);
			return 0;
		}
	}
	if (!isBrave) {
		int runX, runY;
		if (InDanger(field, runX, runY)) {
			Move(field, runX, runY);
			return 0;
		}
	}
	job->Work(field, this);
	return 0;
}

int Ant::DistanceTo(int x1, int y1) {
	//return (int)(sqrt(pow(x - x1, 2) + pow(y - y1, 2)));
	return std::max(abs(x - x1), abs(y - y1));
}

void Ant::GoEat(Cell(&field)[Settings::N][Settings::N]) {
	if (DistanceTo(home->x, home->y <= 2)) {
		int dif = std::min(home->food, Settings::antMaxSatiety - satiety);
		satiety += dif;
		home->food -= satiety;
		return;
	}
	int tx, ty;
	if (FindPath(field, 0, INT_MAX, tx, ty)) {
		Move(field, tx, ty);
	}
}

//mode - looking for: 0 - home, 1 - food, 2 - materials, 3 - other anthills, 4 - input coords
bool Ant::FindPath(Cell (&field)[Settings::N][Settings::N], int mode, int maxDistance, int & toX, int & toY) {
	struct Dot {//dot is just a pathfinding algorithm node
		int x, y;
		int step;
	};
	int moveMap[Settings::N][Settings::N];
	for (int i = 0; i < Settings::N; ++i) {
		for (int j = 0; j < Settings::N; ++j) {
			moveMap[i][j] = INT_MAX;
		}
	}
	std::queue<Dot> current;
	std::queue<Dot> next;
	next.push({ x, y, 0 });
	bool targetFound = false;
	int targetX;
	int targetY;
	while (!next.empty() && !targetFound) {//here wave pathfinding starts
		while (!next.empty()) {
			current.push(next.front());
			next.pop();
		}
		while (!current.empty()) {
			Dot dot = current.front();
			current.pop();
			int tx = dot.x;
			int ty = dot.y;
			//if (dot.step > visibility) continue;//i just forgot about visibility limitation. this line fixs it/
			if (dot.step < maxDistance) {//trying to fix liquid upper line bag using logic dowgrade :/
				for (int tty = ty - 1; tty <= ty + 1 && !targetFound; ++tty) {
					for (int ttx = tx - 1; ttx <= tx + 1 && !targetFound; ++ttx) {
						if (!(ttx == tx && tty == ty)) {
							if (IsValidCoords(ttx, tty)) {
								if (field[ttx][tty].ant == nullptr || (field[ttx][tty].object != nullptr && field[ttx][tty].object->type == 2)) {
									if (moveMap[ttx][tty] > dot.step + 1) {
										moveMap[ttx][tty] = dot.step + 1;
										if (mode == 0 ? ttx == home->x && tty == home->y : (mode == 1 ? field[ttx][tty].object != nullptr && field[ttx][tty].object->type == 0 : (mode == 2 ? field[ttx][tty].object != nullptr && field[ttx][tty].object->type == 1 : (mode == 3 ? field[ttx][tty].object != nullptr && field[ttx][tty].object->type == 2 : (ttx == toX && tty == toY))))) {//if it will ever work properly...
											targetFound = true;
											targetX = ttx;
											targetY = tty;
										} else {
											next.push({ ttx, tty, dot.step + 1 });
										}
									}
								}
							}
						}
					}
				}
			}
			if (targetFound) {
				while (!next.empty()) next.pop();
				while (!current.empty()) current.pop();
			}
		}
	}
	if (targetFound) {
		int tx = targetX;
		int ty = targetY;
		while (moveMap[tx][ty] != 1) {
			bool goNextStep = false;
			for (int tty = ty - 1; tty <= ty + 1 && !goNextStep; ++tty) {
				for (int ttx = tx - 1; ttx <= tx + 1 && !goNextStep; ++ttx) {
					if (!(ttx == tx && tty == ty)) {
						if (IsValidCoords(ttx, tty)) {
							if (moveMap[ttx][tty] == moveMap[tx][ty] - 1) {
								tx = ttx;
								ty = tty;
								goNextStep = true;
							}
						}
					}
				}
			}
		}
		toX = tx - x;
		toY = ty - y;
		return true;
	}
	return false;
}

void Ant::Move(Cell(&field)[Settings::N][Settings::N], int toX, int toY) {
	int tx = x + toX;
	int ty = y + toY;
	bool success = false;
	if (IsValidCoords(x + toX, y + toY)) {
		if (field[x + toX][y + toY].ant == nullptr) {
			field[x + toX][y + toY].ant = this;
			field[x][y].ant = nullptr;
			x = x + toX;
			y = y + toY;
			success = true;
		}
	}
	if (success) return;
	if (IsValidCoords(x, y + toY)) {
		if (field[x][y + toY].ant == nullptr) {
			field[x][y + toY].ant = this;
			field[x][y].ant = nullptr;
			x = x;
			y = y + toY;
			success = true;
		}
	}
	if (success) return;
	if (IsValidCoords(x + toX, y)) {
		if (field[x + toX][y].ant == nullptr) {
			field[x + toX][y].ant = this;
			field[x][y].ant = nullptr;
			x = x + toX;
			y = y;
		}
	}
}

bool Ant::InDanger(Cell(&field)[Settings::N][Settings::N], int & toX, int & toY) {
	int nearestX = Settings::dangerViewRadius * 3;
	int nearestY = Settings::dangerViewRadius * 3;
	bool problems = false;
	for (int ty = y - Settings::dangerViewRadius; ty < y + Settings::dangerViewRadius; ++ty) {
		for (int tx = x - Settings::dangerViewRadius; tx < x + Settings::dangerViewRadius; ++tx) {
			if (IsValidCoords(tx, ty)) {
				if (field[tx][ty].ant != nullptr) {
					if (field[tx][ty].ant->color != color) {
						if (field[tx][ty].ant->isBrave) {
							problems = true;
							if (DistanceTo(tx, ty) < (DistanceTo(nearestX, nearestY))) {
								nearestX = tx;
								nearestY = ty;
							}
						}
					}
				}
			}
		}
	}
	if (problems) {
		nearestX -= x;
		nearestY -= y;
		toX = -(nearestX / (nearestX == 0 ? 1 : abs(nearestX)));
		toY = -(nearestY / (nearestY == 0 ? 1 : abs(nearestY)));
		//std::cout << toX << " " << toY << "   ";
	}
	return problems;
}

