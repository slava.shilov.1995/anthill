#include "Anthill.h"
#include "Ant.h"

#include "Homeless.h"
#include "Getter.h"
#include "Builder.h"
#include "Defender.h"
#include "Warior.h"

#include <iostream>

 Anthill::~Anthill(){
	for (std::vector<Ant*>::iterator it = inhabitants.begin(); it < inhabitants.end(); ++it) {
		delete (*it)->job;
		(*it)->home = nullptr;
		(*it)->isBrave = false;
		(*it)->job = new Homeless();
	}
}

 void Anthill::Interact(Cell(&field)[Settings::N][Settings::N]) {
	 Live(field);
 }

 void Anthill::Live(Cell (&field)[Settings::N][Settings::N]) {
	 int newAnt = lavraStock.Live();
	 if (newAnt == 1) {
		 int tx, ty;
		 int range = 3;
		 do {
			 tx = x - range + rand() % (range * 2 + 1);
			 ty = y - range + rand() % (range * 2 + 1);
		 } while (field[tx][ty].ant != nullptr);
		 if (rand() % 100 == 1) {
			 std::cout << "new queen ";
			 field[tx][ty].ant = new Ant(tx, ty, Settings::startQueenFood, nullptr, nullptr, Settings::antQueenLife);
		 } else {
			 field[tx][ty].ant = new Ant(tx, ty, 100, this, queen == nullptr ? new Queen(this) : GetJob(), queen == nullptr ? Settings::antQueenLife : Settings::antLife);
		 }
		 
	 }
	 --capacity;
 }

 void Anthill::Destroy() {
	 delete this;
 }

 Job * Anthill::GetJob() {
	 float numBuilders = 0;
	 float numDefenders = 0;
	 float numGetters = 0;
	 float numWariors = 0;
	 float numAnts = inhabitants.size();

	 for (std::vector<Ant*>::iterator it = inhabitants.begin(); it < inhabitants.end(); ++it) {
		 switch ((*it)->job->GetType()) {
		 case 1: ++numGetters; break;
		 case 2: ++numBuilders; break;
		 case 3: ++numDefenders; break;
		 case 4: ++numWariors; break;
		 }
	 }
	 if (numBuilders + numDefenders + numGetters + numWariors != numAnts - 1) {
		 std::cout << "Something wrong.. ";
	 }

	 numBuilders /= numAnts;
	 numDefenders /= numAnts;
	 numGetters /= numAnts;
	 numWariors /= numAnts;
	
	 if (numGetters <= 0.5) return new Getter;
	 if (numBuilders <= 0.3) return new Builder;

	 return new Getter;
 }