#pragma once
#include "Cell.h"
#include "Settings.h"
class Field {
	
public:
	Cell field[Settings::N][Settings::N];
	
	Field();
	void Step();

	void GetFreeCoords(int matrixIndex, int & x, int & y);//0 - ants matrix, 1 - objects matrix; returns coords into referenced vars

	//void AddFood(int _x, int _y, int caloricity) {};
	//void AddMaterial(int _x, int _y, int strength) {};
	//void AddAnt(int _x, int _y, int age) {};
	//void AddAnthill(int _x, int _y, int age) {};
};