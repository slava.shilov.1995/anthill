#include "LavraStock.h"

void LavraStock::Add() {
	stock.push_back(Lavra());
}

int LavraStock::Live() {
	for (std::vector<Lavra>::iterator it = stock.begin(); it < stock.end(); ++it) {
		(*it).Live();
		if ((*it).getAge() <= 0) {
			stock.erase(it);
			return 1;
		}
	}
	return 0;
}