#pragma once
#include "Settings.h"

class Lavra {
public:
	int age;

	Lavra() {
		age = Settings::lavraLiveTime;
	}
	~Lavra() {};

	void Live() {
		age > 0 ? --age : age;
	}
	int getAge() {
		return age;
	}
};