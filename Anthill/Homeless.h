#pragma once

#include "Job.h"

class Homeless : public Job {
private:
	
public:
	virtual int Work(Cell(&field)[Settings::N][Settings::N], Ant * me);
	Homeless() {};
	~Homeless() {};
	virtual void Destroy() { delete this; }
	virtual int GetType() { return -1; }
};