#pragma once
#include"Object.h"
class Material : public Object{
public:
	int strength;

	Material(int _x, int _y) {
		x = _x;
		y = _y;
		type = 1;
		strength = Settings::materialMinQuantity + rand() % Settings::materialRandomRange;
	}
	Material(int _x, int _y, int _strength) {
		x = _x;
		y = _y;
		type = 1;
		strength = _strength;
	}
	~Material() {};

	void Interact(Cell(&field)[Settings::N][Settings::N]) {}
	int getMass() { return strength; }
	void Destroy() { delete this; }
};