#pragma once
#include "Job.h"
class Builder : public Job {
private:
	int materialCount;

private:
	
public:
	Builder() {
		materialCount = 0;
	}

	virtual int Work(Cell(&field)[Settings::N][Settings::N], Ant * me);
	virtual void Destroy() { delete this; }
	virtual int GetType() { return 2; }
};