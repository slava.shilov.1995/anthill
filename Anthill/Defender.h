#pragma once
#include "Job.h"
class Defender : public Job {
private:
	
public:
	virtual int Work() {};
	int CheckDanger() {};
	int Defend() {};
	virtual void Destroy() { delete this; }
	virtual int GetType() { return 3; }
};