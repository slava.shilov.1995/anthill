#pragma once
#include "Object.h"
//#include "Job.h"
#include "LavraStock.h"

#include <SFML/Graphics.hpp>
class Ant;
class Job;

class Anthill : public Object {
public:
	int capacity;
	int food;

	std::vector<Ant*> inhabitants;
	LavraStock lavraStock;
	Ant* queen;

	sf::Color color;

	Anthill(int _x, int _y) {
		x = _x;
		y = _y;
		type = 2;
	}
	~Anthill();

	void Interact(Cell(&field)[Settings::N][Settings::N]);
	int getMass() { return capacity; }
	void Live(Cell(&field)[Settings::N][Settings::N]);

	Job * GetJob();

	void Destroy();
};